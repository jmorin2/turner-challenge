﻿
function searchByText() {
  
    var searchValue = $("#SearchText").val();
    $("#AjaxWaitingSection").css("visibility", "visible");

    $.ajax({
        type: "POST",
        url: '/Home/GetTitleDetailByText',
        data: { searchValue: searchValue },
        success: searchSuccess,
        dataType: "JSON"
    });
}

function titleClickByID(titleID) {

    $("#AjaxWaitingSection").css("visibility", "visible");
    $.ajax({
        type: "POST",
        url: '/Home/GetTitleDetailByID',
        data: { id: titleID },
        success: searchSuccess,
        dataType: "JSON"
    });
}

function searchSuccess(data) {

    $("#AjaxWaitingSection").css("visibility", "hidden");
    $("#SearchText").html("");
    clearData();

    if (data != null && data.Exists) {
       
        $("#TitleName").html(data.TitleName);
        $("#ReleaseYear").html(data.ReleaseYear);

        if (data.Awards != null && data.Awards.length > 0) {
          
            var tableValues = "";
            for (var i = 0; i < data.Awards.length; i++) {
                if (data.Awards[i].AwardWon) {

                    tableValues += "<tr><td>" + data.Awards[i].Award + " (" + data.Awards[i].AwardYear +  ")</td></tr>";                    
                }
            }

            $("#AwardsTable").html(tableValues);
            $("#Awards").show();
        }

        if (data.Genres != null && data.Genres.length > 0) {
           
            var tableValues = "";
            for (var i = 0; i < data.Genres.length; i++) {
                tableValues += "<tr><td>" + data.Genres[i] + i + "</td><td>";
            }

            $("#GenresTable").html(tableValues);
            $("#Genres").show();
        }

        if (data.OtherNames != null && data.OtherNames.length > 0) {

            var tableValues = "";
            for (var i = 0; i < data.OtherNames.length; i++) {

                tableValues += "<tr><td class='innerLabel'>Other Name: </td><td> " + data.OtherNames[i].TitleName + "</td></tr>";
                tableValues += "<tr><td class='innerLabel'>Language: </td><td>" + data.OtherNames[i].TitleNameLanguage + "</td></tr>";
                tableValues += "<tr><td class='innerLabel bottomRow'>Type: </td><td>" + data.OtherNames[i].TitleNameType + "</td></tr>";
            }

            $("#OtherNamesTable").html(tableValues);
            $("#OtherNames").show();
        }
        
        $("#Results").show();
        $("#NoResultsFound").hide();
    }
    else {
        $("#Results").hide();
        $("#NoResultsFound").show();
    }
}

function clearData() {

    $("#Awards").hide();
    $("#Genres").hide();
    $("#OtherNames").hide();

    $("#AwardsTable").html();
    $("#GenresTable").html();
    $("#OtherNames").html();
}
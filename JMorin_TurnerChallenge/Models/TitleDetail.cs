﻿using System.Collections.Generic;

namespace JMorin_TurnerChallenge.Models
{
    public class TitleDetail
    {
        public int TitleID { get; set; }

        public string TitleName { get; set; }

        public string ReleaseYear { get; set; }

        public List<AwardInfo> Awards { get; set; }

        public List<string> Genres { get; set; }

        public List<OtherNameInfo> OtherNames { get; set; }

        public bool Exists { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JMorin_TurnerChallenge.Models
{
    public class OtherNameInfo
    {
        public string TitleNameLanguage { get; set; }

        public string TitleNameType { get; set; }

        public string TitleName { get; set; }
    }
}
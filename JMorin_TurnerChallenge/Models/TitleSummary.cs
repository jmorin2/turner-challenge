﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JMorin_TurnerChallenge.Models
{
    public class TitleSummary
    {
        public int TitleID { get; set; }

        public string TitleName { get; set; }

        public string ReleaseYear { get; set; }
    }
}
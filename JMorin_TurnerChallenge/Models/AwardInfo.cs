﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JMorin_TurnerChallenge.Models
{
    public class AwardInfo
    {
        public bool AwardWon { get; set; }

        public string AwardYear { get; set; }

        public string Award { get; set; }

        public string AwardCompany { get; set; }
    }
}
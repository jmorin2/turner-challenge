﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using JMorin_TurnerChallenge.Models;

namespace JMorin_TurnerChallenge.Data
{
    public class DataAccessor
    {
        private string _connection_string = ConfigurationManager.ConnectionStrings["BooksConnectionString"].ToString();

        public List<TitleSummary> GetTitleSummaries(string searchField)
        {
            using (LinqDataDataContext context = new LinqDataDataContext(_connection_string))
            {
                List<TitleSummary> results = (from t in context.Titles
                                             select new TitleSummary()
                                             {
                                                TitleName = t.TitleName,
                                                TitleID = t.TitleId,
                                                ReleaseYear = t.ReleaseYear != null ? t.ReleaseYear.ToString() : "Not Available"   
                                             }).ToList();

                return results;
            }
        }

        public TitleDetail GetTitleDetailByName(string name)
        {
            using (LinqDataDataContext context = new LinqDataDataContext(_connection_string))
            {
                int titleID = (from t in context.Titles
                               where t.TitleName.ToLower() == name.ToLower()
                               select t.TitleId).FirstOrDefault();

                return GetTitleDetailByID(titleID);
            }
        }

        public TitleDetail GetTitleDetailByID(int titleID)
        {
            using (LinqDataDataContext context = new LinqDataDataContext(_connection_string))
            {         
                TitleDetail result = (from t in context.Titles
                               where t.TitleId == titleID
                               select new TitleDetail()
                               {
                                   TitleID = t.TitleId,
                                   TitleName = t.TitleName,
                                   ReleaseYear = t.ReleaseYear != null ? t.ReleaseYear.ToString() : "Not Available"
                               }).FirstOrDefault();


                if (result != null)
                {
                    result.Awards = (from a in context.Awards
                                     where a.TitleId == result.TitleID
                                     select new AwardInfo()
                                     {
                                         Award = a.Award1 != null ? a.Award1 : "Not available",
                                         AwardWon = a.AwardWon != null ? Convert.ToBoolean(a.AwardWon) : false,
                                         AwardYear = a.AwardYear != null ? a.AwardYear.ToString() : "Not available",
                                         AwardCompany = a.AwardCompany != null ? a.AwardCompany : "Not available"

                                     }).ToList();

                    result.OtherNames = (from o in context.OtherNames
                                         where o.TitleId == result.TitleID
                                         select new OtherNameInfo()
                                         {
                                             TitleName = o.TitleName,
                                             TitleNameLanguage = o.TitleNameLanguage,
                                             TitleNameType = o.TitleNameType
                                         }).ToList();

                    result.Genres = (from tg in context.TitleGenres
                                     join g in context.Genres on tg.GenreId equals g.Id
                                     where tg.TitleId == result.TitleID
                                     select g.Name).ToList();

                    result.Exists = true;

                }
                else
                {
                    result = new TitleDetail() { Exists = false };
                }

                return result;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JMorin_TurnerChallenge.Data;
using JMorin_TurnerChallenge.Models;

namespace JMorin_TurnerChallenge.Controllers
{
    public class HomeController : Controller
    {
        DataAccessor _data_accessor = new DataAccessor();

        public ActionResult Titles()
        {
            //sort by name
            List<TitleSummary> summary = _data_accessor.GetTitleSummaries(string.Empty).OrderBy(t => t.TitleName).ToList();
            return View(summary);
        }

        public JsonResult GetTitleDetailByText(string searchValue)
        {
            TitleDetail detail = _data_accessor.GetTitleDetailByName(searchValue);
            return Json(detail);
        }

        public JsonResult GetTitleDetailByID(int id)
        {
            TitleDetail detail = _data_accessor.GetTitleDetailByID(id);
            return Json(detail);
        }
    }
}
